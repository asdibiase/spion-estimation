#%%

import io
import sys

from numba import njit, prange
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage
from skimage.filters import gaussian


def racian_noise(size, std=1) -> np.ndarray:
    return np.sqrt((std*np.random.normal(size=size))**2 + \
                   (std*np.random.normal(size=size))**2 )

# @njit()
def normalize(x) -> np.ndarray:
    """Normalizes the input from 0 to 1.
    """
    return (x - np.min(x)) / (np.max(x) - np.min(x) + 1e-9)

# @njit()
def map(x, in_max=None, in_min=None, out_max=1.0, out_min=0.0) -> np.ndarray:
    if in_max is None:
        in_max = np.max(x)
    if in_min is None:
        in_min = np.min(x)
    return (x - in_min) * (out_max - out_min) / (in_max - in_min + 1e-9) + out_min

# @njit()
def cylinder(h, r, pos, X, Y, Z) -> np.ndarray:
    # return 1*( ( (X-pos[0])/r )**2 + ( (Y-pos[1])/r )**2 < 1 )
    return 1.0*np.logical_and( ( (X-pos[0])/r )**2 + ( (Y-pos[1])/r )**2 < 1, \
            (np.abs(Z-pos[2]) < h/2))

# @njit(parallel=True)
def smooth_bg(X, Y, Z, FOV) -> np.ndarray:
    pos0 = np.random.randn(3)*FOV/6 + FOV/2
    s0 = 1*np.random.rand(3)
    pos1 = np.random.randn(3)*FOV/6 + FOV/2
    s1 = 1*np.random.rand(3)
    # print(FOV, X.shape, X.max(), Y.max(), Z.max())
    # print(pos0, pos1)
    smooth= ((X - pos0[0])*s0[0])**2 + ((Y - pos0[1])*s0[1])**2 + ((Z - pos0[2])*s0[2])**2 + \
            ((X - pos1[0])*s1[0])**3 + ((Y - pos1[1])*s1[1])**3 + ((Z - pos1[2])*s1[2])**3
    return smooth

def bg(N):
    out = np.random.randn(*N)
    return ndimage.gaussian_filter(out, sigma=50)

@njit()
def metaball(pos, scale, X, Y, Z, theta=0, psi=0) -> np.ndarray:
    # rotation in the z direction
    Xp = (X-pos[0])*np.cos(theta) + (Y-pos[1])*np.sin(theta)
    Yp = (X-pos[0])*np.sin(theta) - (Y-pos[1])*np.cos(theta)
    # rotation in the x direction
    Ypp = (Z-pos[2])*np.cos(psi) + Yp*np.sin(psi)
    Zp = (Z-pos[2])*np.sin(psi) - Yp*np.cos(psi)
    return 1 / ( np.sqrt( (Xp**2)*scale[0] + \
                          (Ypp**2)*scale[1] + \
                          (Zp**2)*scale[2] ) + 1e-12)

# @njit()
def sphere(pos, scale, X, Y, Z, theta=0, psi=0) -> np.ndarray:
    # rotation in the z direction
    Xp = (X-pos[0])*np.cos(theta) + (Y-pos[1])*np.sin(theta)
    Yp = (X-pos[0])*np.sin(theta) - (Y-pos[1])*np.cos(theta)
    # rotation in the x direction
    Ypp = (Z-pos[2])*np.cos(psi) + Yp*np.sin(psi)
    Zp = (Z-pos[2])*np.sin(psi) - Yp*np.cos(psi)
    return 1 > np.sqrt( (Xp/scale[0])**2 + \
                        (Ypp/scale[1])**2 + \
                        (Zp/scale[2])**2 )

@njit()
def spio_map_metaball(num, scale, X, Y, Z, FOV, threshold):
    randn = np.random.randn
    rand = np.random.rand
    blob = np.zeros_like(X)
    # print(FOV, scale)
    for i in range(num):
        pos = ((FOV/6))*randn(3) + FOV/2
        pos[2] = FOV[2]*rand()
        s = np.abs(scale + 0.333*randn(3))
        c = rand()
        potential = np.zeros_like(blob)
        for _ in range(int(10*rand())):
            potential += metaball(pos+0.0001*randn(), s, X, Y, Z, rand()*2*np.pi, rand()*2*np.pi)
        blob += c * (potential > threshold)
        # print(pos, s, c)

    return blob

@njit()
def spio_map_rings(num, scale, X, Y, Z, FOV, threshold):
    blob = np.zeros(X.shape)
    randn = np.random.randn
    rand = np.random.rand
    blob = np.zeros_like(X)
    # anti_blob = np.zeros_like(X)
    # print(FOV, scale)
    for i in range(num):
        pos = ((FOV/6))*randn(3) + FOV/2
        pos[2] = FOV[2]*rand()

        s = np.abs(scale + 0.333*randn(3))
        c = rand()
        potential = np.zeros_like(blob)
        for _ in range(int(10*rand())):
            potential += metaball(pos+0.0001*randn(), s, X, Y, Z, rand()*2*np.pi, rand()*2*np.pi)
        blob +=  c * (np.logical_and(potential > threshold, potential < 1000*rand() + threshold))
        # anti_blob -= c*metaball(pos, 1*s, X, Y, Z)

    return blob

@njit()
def spio_map(num, scale, X, Y, Z, FOV, threshold):

    rings = spio_map_rings(num, scale, X, Y, Z, FOV, threshold)
    rings += spio_map_metaball(num, scale, X, Y, Z, FOV, threshold)
    return rings

class progressbar():

    def __init__(self, it, size=60, prefix="", file=sys.stdout):
        self.it = it
        self.size = size
        self.file = file
        self.prefix = prefix
        self.count = len(it)
        self.index = 0

    def show(self):
        x = int(self.size*self.index/self.count)
        self.file.write(self.prefix +
                "\r[{}{}]{}/{}".format("#"*x, "."*(self.size-x), self.index, self.count))
        self.file.flush()

    def __call__(self):
        self.show()
        for i in self.it:
            self.index += 1
            yield i
            self.show()

        self.file.write("\n")
        self.file.flush()

def center_of_mass(x):
    return ndimage.measurements.center_of_mass(x)


def plot_to_image(figure):
    """Converts the matplotlib plot specified by 'figure' to a PNG image and
    returns it. The supplied figure is closed and inaccessible after this call."""

    import tensorflow as tf

    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    # Closing the figure prevents it from being displayed directly inside
    # the notebook.
    plt.close(figure)
    buf.seek(0)
    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    # Add the batch dimension
    image = tf.expand_dims(image, 0)
    return image

def plot_3d(x, point, title="", second=None):
    fig, ax = plt.subplots(2, 2)
    fig.suptitle(title)

    axial_img = x[:, :, point[2]]
    coronal_img = x[:, point[1], :]
    saggital_img = x[point[0], :, :]

    max_value = max(axial_img.max(), coronal_img.max(), saggital_img.max())
    min_value = min(axial_img.min(), coronal_img.min(), saggital_img.min())

    im = ax[0, 0].imshow(axial_img, cmap='gray', interpolation='nearest', aspect='auto', vmin=min_value, vmax=max_value)
    # ax[0].set_xticks([])
    # ax[0].set_yticks([])
    ax[0, 1].imshow(coronal_img, cmap='gray', interpolation='nearest', aspect='auto', vmin=min_value, vmax=max_value)
    # ax[1].set_xticks([])
    # ax[1].set_yticks([])
    ax[1, 0].imshow(np.rot90(saggital_img), cmap='gray', interpolation='nearest', aspect='auto', vmin=min_value, vmax=max_value)
    # ax[2].set_xticks([])
    # ax[2].set_yticks([])
    ax[1, 1].set_axis_off()
    ax[1, 1].text(0.5, 0.5, str(point))
    fig.colorbar(im, ax=ax[1, 1], location='left')

    if type(second) is np.ndarray:
        axial_img = second[:, :, point[2]]
        coronal_img = second[:, point[1], :]
        saggital_img = second[point[0], :, :]

        max_value = max(axial_img.max(), coronal_img.max(), saggital_img.max())
        min_value = min(axial_img.min(), coronal_img.min(), saggital_img.min())

        im = ax[0, 0].imshow(axial_img, alpha=0.5, interpolation='nearest', aspect='auto', vmin=min_value, vmax=max_value)
        # ax[0].set_xticks([])
        # ax[0].set_yticks([])
        ax[0, 1].imshow(coronal_img, alpha=0.5, interpolation='nearest', aspect='auto', vmin=min_value, vmax=max_value)
        # ax[1].set_xticks([])
        # ax[1].set_yticks([])
        ax[1, 0].imshow(np.rot90(saggital_img), alpha=0.5, interpolation='nearest', aspect='auto', vmin=min_value, vmax=max_value)
        # ax[1, 1].set_axis_off()
        # ax[1, 1].text(0.5, 0.5, str(point))
        fig.colorbar(im, ax=ax[1, 1], location='left')


    fig.tight_layout()


#%%
if __name__ == '__main__':
    from skimage.util import montage

    N = np.asarray([256, 256, 25])
    ds = np.asarray([0.195, 0.195, 1]) # voxel size (mm)
    FOV = ds * N
    print(FOV)
    X, Y, Z = np.meshgrid(np.linspace(0, FOV[0], N[0]), \
                            np.linspace(0, FOV[1], N[1]), \
                            np.linspace(0, FOV[2], N[2]) )
    a = smooth_bg(X, Y, Z, FOV)
    plot_3d(a, [128, 128, 14])
    plt.show()
    # plt.imshow(montage(a.T, rescale_intensity=True))
    # plt.colorbar()
    # plt.show()

# %%
