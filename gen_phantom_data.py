import argparse
import os
import pathlib

import h5py
import numpy as np
import matplotlib.pyplot as plt
from skimage.util import montage
from skimage.transform import resize
from skimage.morphology import erosion
from skimage.io import imsave
import tqdm
from numba import jit, njit, prange

from simulator import *
from utils import *


# @njit(parallel=True)
def object(X, Y, Z, FOV):
    # return 1*cylinder(FOV[2], FOV[0], FOV/2, X, Y, Z)
    scale = (FOV/2)*(np.random.rand(3) + 0.5)
    scale[2] = FOV[2]/1.5
    obj = 1.0*sphere(FOV/2, scale, X, Y, Z, 0, np.pi/2)
    const = (2*np.random.rand()-1)*sphere((FOV/12)*np.random.randn(3)+FOV/2,
                            np.random.rand(3)*FOV/3,
                            X, Y, Z,
                            np.random.rand()*2*np.pi, np.random.rand()*2*np.pi)
    for _ in range(np.random.randint(1, 10)):
        const += (np.random.rand())*sphere((FOV/12)*np.random.randn(3)+FOV/2,
                                    np.random.rand(3)*FOV/3,
                                    X, Y, Z,
                                    np.random.rand()*2*np.pi, np.random.rand()*2*np.pi)

    const.ravel()[obj.ravel() < 1] = 0.0
    const = np.maximum(const, 0)
    obj += const

    return obj

def save_data(save_dir, i, dist1, dist2,  conc, image, fieldmap, Gz):

    with h5py.File(save_dir / '{:06d}.h5'.format(i), 'w') as savefile:
        savefile.create_dataset("positive_grad", data=dist1)
        savefile.create_dataset("negative_grad", data=dist2)
        savefile.create_dataset("concentration", data=conc)
        savefile.create_dataset("original_image", data=image)
        savefile.create_dataset("fieldmap", data=fieldmap)
        savefile.attrs['gradient'] = Gz

    if SHOW:

        p = [0 if np.isnan(i) else round(i) for i in center_of_mass(conc)]
        if p == [0, 0, 0]:
            p = [128, 128, 14]
        print(p)
        plot_3d(dist1, p, "pos grad")
        plot_3d(dist2, p, "neg grad")
        plot_3d(dist1 - dist2, p, "difference")
        plot_3d(conc, p, "conc")
        # plt.show()
        plot_3d(fieldmap, p, "fieldmap")


        # plt.figure()
        # plt.subplot(121)
        # plt.title("Positive grad")
        # plt.imshow(montage((dist1).T), cmap='gray')
        # plt.colorbar()
        # plt.subplot(122)
        # plt.title("Negative grad")
        # plt.imshow(montage((dist2).T), cmap='gray')
        # plt.colorbar()
        # # plt.show()
        # plt.figure()
        # plt.title("Diff")
        # plt.imshow(montage((dist2 - dist1).T), cmap='gray')
        # plt.colorbar()
        # # plt.show()
        plt.figure()
        plt.title("fieldmap")
        plt.imshow(montage(fieldmap.T), cmap='gray')
        plt.colorbar()
        # plt.show()
        # plt.figure()
        # plt.imshow(montage(conc.T), cmap='gray')
        # plt.colorbar()
        # plt.title("conc")
        plt.show()
        # imsave("pics/input.png", (dist1 - dist2)[:,:, p[2]])
        # imsave("pics/neg.png", dist1[:, :, p[2]])
        # imsave("pics/pos.png", dist2[:, :, p[2]])
        # imsave("pics/output.png", conc[:, :, p[2]])
        # imsave("pics/fm.png", fieldmap[:, :, int((256/25)*p[2])])
        # imsave("pics/img.png", image[:, :, int((256/25)*p[2])])


def make_data(NUM, start):

    N = np.asarray([256, 256, 256])
    FOV = np.asarray([0.050, 0.050, 0.025]) # m
    ds = FOV / N

    Gz = (440*10**-3) # T/m

    xx = np.linspace(0, FOV[0], N[0])
    yy = np.linspace(0, FOV[1], N[1])
    zz = np.linspace(0, FOV[2], N[2])
    X, Y, Z = np.meshgrid(xx, yy, zz)

    sim = Simulator(N, ds, Z)

    for i in range(start, start+NUM):

        image = object(X, Y, Z, FOV)
        qty = np.random.randint(15, 30)
        blob = spio_map(qty, np.asarray([1.0, 1.0, 1.0]), X, Y, Z, FOV, 2500)

        # remove SPIO outside tissue
        blob[image == 0] = 0

        for _ in range(5):
            blob = erosion(blob)
        # remove tissue where SPIO is present
        blob = normalize(blob)
        image[blob > 0] = 0


        if SHOW:
            p = [0 if np.isnan(i) else round(i) for i in center_of_mass(blob)]
            if p == [0, 0, 0]:
                p = [128, 128, 128]
            plt.imshow(montage(image.T), cmap='gray')
            plt.colorbar()
            plot_3d(image, p)
            plt.figure()
            plt.imshow(montage(blob.T), cmap='gray')
            plt.colorbar()


        # we should rotate both images to make the problem not as trivial as substraction
        fieldmap = normalize(sim.get_fieldmap(blob))
        w0_ppm = np.random.uniform(low=0, high=10)
        # print(w0_ppm)
        fieldmap = map(fieldmap, out_max=w0_ppm, out_min=-w0_ppm)
        smooth = smooth_bg(X, Y, Z, FOV)
        # smooth = bg(N)
        fieldmap += map(smooth, out_max=0.5, out_min=-0.5)

        dist1 = sim.get_extSelectVol(Gz, fieldmap, image, 25)
        dist2 = sim.get_extSelectVol(-Gz, fieldmap, image, 25)

        # if SHOW:
            # dw = np.abs(GAMMA*Gz*ds[2]*N[2])/(25) # Hz # range of exited frequencies
            # mrf, wrf, k = extSelect(Gz, fieldmap, image, 0.5, ds*N, dw, Z)
            # plt.figure()
            # plt.title("Excitation profile")
            # plt.imshow(montage(wrf))


        conc = 27.9/10 * w0_ppm * blob
        conc = resize(conc, (N[0], N[1], 25))
        # blob1 = resize(blob, (N[0], N[1], 25))
        # blob2 = blob[:, :, np.arange(0, N[0], N[0]/25, dtype='int')]

        # add ricean noise
        # dist1 += rice.rvs(0, scale=0.001, size=N)
        # dist2 += rice.rvs(0, scale=0.001, size=N)

        save_data(save_dir, i, dist1, dist2, conc, image, fieldmap, Gz)
        print("{}/{}".format(i+1, NUM))

        del dist1, dist2, conc, image, fieldmap, blob, smooth

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--save-dir",
                        help="Path as the data",
                        required=True,
                        type=pathlib.Path)
    parser.add_argument("--num",
                        help="Number of images to generate",
                        default=1,
                        type=int)
    parser.add_argument("--show",
                        help="Wherever to plot intermidiate images. Usefull for debugging",
                        action='store_true')
    parser.add_argument("--start",
                        help="start number of volumes",
                        default=0,
                        type=int)

    args = parser.parse_args()
    SHOW = args.show

    save_dir = args.save_dir
    save_dir.mkdir(exist_ok=True, parents=True)
    make_data(args.num, args.start)
