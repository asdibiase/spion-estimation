import pathlib

import matplotlib.pyplot as plt
import numpy as np
import h5py
import tensorflow as tf
from tensorflow.keras.utils import Sequence
from tqdm import tqdm
from skimage.transform import resize

from utils import progressbar, normalize, map

mean_x = -2.875473556407448e-05
std_x = 0.6597405099099085

mean_y = 0.04995233171081057
std_y = 0.7030072369353082

class DataGenerator(Sequence):
    'Generates data for Keras'
    def __init__(self, file_path, batch_size=32, dim=(320, 320, 2), shuffle=True):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.file_path = pathlib.Path(file_path)
        self.files = list(self.file_path.glob('*.h5'))
        i = 0
        self.examples = []
        for fname in progressbar(self.files)():
            with h5py.File(fname, "r") as f:
                num_slices = f['concentration'].shape[-1]
            self.examples += [(fname, slice, i)
                          for slice in range(num_slices)]
            i = i + 1

        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        # uses 20 slices per volume
        return len(self.examples)

    def __getitem__(self, index):
        'Generate one batch of data' # X : (n_samples, *dim, n_channels)
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        tmp_files = [self.examples[k] for k in indexes]

        # Generate data
        X = np.empty((self.batch_size,) + self.dim, dtype=np.float32)
        y = np.empty((self.batch_size,) + self.dim[:-1] + (1,), dtype=np.float32)
        for i, tmp in enumerate(tmp_files):
            tmp_fname, s, _ = tmp
            X[i], y[i] = self.__data_generation(tmp_fname, s)

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.examples))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, fname, slice_):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)

        with h5py.File(fname, "r") as f:
            mVolpos = f['positive_grad'][..., slice_]
            mVolneg = f['negative_grad'][..., slice_]
            conc = f['concentration'][..., slice_]

            mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
            mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
            # conc = (conc - np.mean(conc)) / np.std(conc)

        return np.stack([mVolpos, mVolneg], axis=-1), conc[..., np.newaxis]


def load_single_volume(file_path, dim):
    file_path = pathlib.Path(file_path)
    files = list(file_path.glob('*.h5'))

    single_file = np.random.choice(files)
    x = []
    y = []
    with h5py.File(single_file, "r") as f:
        num_slices = f['concentration'].shape[-1]
        for j in range(1, num_slices-1):
            mVolpos = f['positive_grad'][..., j-1:j+2]
            mVolneg = f['negative_grad'][..., j-1:j+2]
            conc = f['concentration'][..., j]
            mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
            mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
            # conc = (conc - np.mean(conc)) / np.std(conc)

            x.append(np.asarray([mVolneg, mVolpos]))
            y.append(conc)

    x = np.asarray(x)
    y = np.asarray(y)
    return x, y

def load_difference(file_path, cache=False, noise=False,  bar=True):
    noise_name = "_noise_" if noise else ""
    if cache:
        folder = pathlib.Path(file_path)
        if (folder / "cache_x_dif.npy").is_file():
            x = np.load(folder / f"{noise_name}cache_x_dif.npy")
            y = np.load(folder / f"{noise_name}cache_y_dif.npy")
            print(f"Loaded data with shapes: {x.shape}, {y.shape}")
            return x, y

    file_path = pathlib.Path(file_path)
    files = list(file_path.glob('*.h5'))
    # print(file_path)
    x = []
    y = []

    for fname in tqdm(files, disable=(not bar)):
        # for each volume generate a 3 channel image
        with h5py.File(fname, "r") as f:

            # print(f['concentration'].shape)
            num_slices = f['concentration'].shape[-1]
            pos = normalize(np.asarray(f['positive_grad']))
            neg = normalize(np.asarray(f['negative_grad']))
            # print(fname)
            # if noise:
            #     snr = 0.001
            #     # print(snr)
            #     pos += np.sqrt(np.random.normal(scale=snr, size=pos.shape)**2 + \
            #                    np.random.normal(scale=snr, size=pos.shape)**2)
            #     neg += np.sqrt(np.random.normal(scale=snr, size=neg.shape)**2 + \
            #                    np.random.normal(scale=snr, size=neg.shape)**2)

            # dif_vol = pos - neg
            dif_vol = normalize(neg - pos)

            for j in range(1, num_slices-1):
                # mVolpos = pos[..., j-1:j+2]
                # mVolneg = neg[..., j-1:j+2]
                dif = dif_vol[..., j-1:j+2]
                if dif.shape != (256, 256, 3):
                    continue
                conc = f['concentration'][..., j]
                if np.any(np.logical_not(np.isfinite(dif))):
                    print("Nan")
                dif[np.logical_not(np.isfinite(dif))] = 0
                if noise:
                    snr = 0.05
                    dif += np.random.normal(scale=snr, size=dif.shape)
                # mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
                # mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
                # conc = (conc - np.mean(conc)) / np.std(conc)

                # dif = mVolneg - mVolpos


                # dif = (dif - np.mean(dif)) / np.std(dif)

                x.append(dif)
                y.append(conc)
    # print([i.shape for i in x])
    x = np.asarray(x)
    y = np.asarray(y)
    print(f"Loaded data with shapes: {x.shape}, {y.shape}")

    if cache:
        np.save(folder / f"{noise_name}cache_x_dif", x)
        np.save(folder / f"{noise_name}cache_y_dif", y)

    return x, y


def load_difference_single_slice(file_path, cache=False, noise=False,  bar=True):
    noise_name = "_noise_" if noise else ""
    if cache:
        folder = pathlib.Path(file_path)
        if (folder / "cache_x_dif.npy").is_file():
            x = np.load(folder / f"{noise_name}cache_x_dif.npy")
            y = np.load(folder / f"{noise_name}cache_y_dif.npy")
            print(f"Loaded data with shapes: {x.shape}, {y.shape}")
            return x, y

    file_path = pathlib.Path(file_path)
    files = list(file_path.glob('*.h5'))
    # print(file_path)
    x = []
    y = []

    for fname in tqdm(files, disable=(not bar)):
        # for each volume generate a 3 channel image
        with h5py.File(fname, "r") as f:

            # print(f['concentration'].shape)
            num_slices = f['concentration'].shape[-1]
            pos = normalize(np.asarray(f['positive_grad']))
            neg = normalize(np.asarray(f['negative_grad']))
            # print(fname)
            # if noise:
            #     snr = 0.001
            #     # print(snr)
            #     pos += np.sqrt(np.random.normal(scale=snr, size=pos.shape)**2 + \
            #                    np.random.normal(scale=snr, size=pos.shape)**2)
            #     neg += np.sqrt(np.random.normal(scale=snr, size=neg.shape)**2 + \
            #                    np.random.normal(scale=snr, size=neg.shape)**2)

            # dif_vol = pos - neg
            dif_vol = normalize(neg - pos)

            for j in range(num_slices):
                # mVolpos = pos[..., j-1:j+2]
                # mVolneg = neg[..., j-1:j+2]
                dif = dif_vol[..., j]
                dif = dif[..., np.newaxis]
                if dif.shape != (320, 320, 1):
                    continue
                conc = f['concentration'][..., j]
                if np.any(np.logical_not(np.isfinite(dif))):
                    print("Nan")
                dif[np.logical_not(np.isfinite(dif))] = 0
                if noise:
                    snr = 0.05
                    dif += np.random.normal(scale=snr, size=dif.shape)
                # mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
                # mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
                # conc = (conc - np.mean(conc)) / np.std(conc)

                # dif = mVolneg - mVolpos


                # dif = (dif - np.mean(dif)) / np.std(dif)

                x.append(dif)
                y.append(conc)
    # print([i.shape for i in x])
    x = np.asarray(x)
    y = np.asarray(y)
    print(f"Loaded data with shapes: {x.shape}, {y.shape}")

    if cache:
        np.save(folder / f"{noise_name}cache_x_dif", x)
        np.save(folder / f"{noise_name}cache_y_dif", y)

    return x, y


def load_difference_eval(file_path, cache=False, noise=False,  bar=True, size=(256, 256)):
    if cache:
        folder = pathlib.Path(file_path)
        if (folder / "cache_x_dif.npy").is_file():
            x = np.load(folder / "cache_x_dif.npy")
            y = np.load(folder / "cache_y_dif.npy")
            print(f"Loaded data with shapes: {x.shape}, {y.shape}")
            return x, y

    file_path = pathlib.Path(file_path)
    files = sorted(list(file_path.glob('*.h5')))
    # print(file_path)
    x = []
    y = []
    m = []

    for fname in tqdm(files, disable=(not bar)):
        # for each volume generate a 3 channel image
        with h5py.File(fname, "r") as f:

            # print(f['concentration'].shape)
            num_slices = f['concentration'].shape[-1]
            pos = normalize(np.asarray(f['positive_grad']))
            neg = normalize(np.asarray(f['negative_grad']))
            # print(fname)
            # if noise:
            #     snr = 0.001
            #     # print(snr)
            #     pos += np.sqrt(np.random.normal(scale=snr, size=pos.shape)**2 + \
            #                    np.random.normal(scale=snr, size=pos.shape)**2)
            #     neg += np.sqrt(np.random.normal(scale=snr, size=neg.shape)**2 + \
            #                    np.random.normal(scale=snr, size=neg.shape)**2)

            # dif_vol = pos - neg
            dif_vol = normalize(neg - pos)

            for j in range(1, num_slices-1):
                # mVolpos = pos[..., j-1:j+2]
                # mVolneg = neg[..., j-1:j+2]
                dif = dif_vol[..., j-1:j+2]
                dif = resize(dif, size)
                img = neg[..., j-1:j+2]
                img = resize(img, size)

                if dif.shape != size +  (3, ):
                    continue
                conc = f['concentration'][..., j]
                if np.any(np.logical_not(np.isfinite(dif))):
                    print("Nan")
                dif[np.logical_not(np.isfinite(dif))] = 0
                if noise:
                    snr = 0.0000025
                    dif += np.random.normal(scale=snr, size=dif.shape)
                # mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
                # mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
                # conc = (conc - np.mean(conc)) / np.std(conc)

                # dif = mVolneg - mVolpos


                # dif = (dif - np.mean(dif)) / np.std(dif)

                x.append(dif)
                y.append(conc)
                m.append(img)
    # print([i.shape for i in x])
    x = np.asarray(x)
    y = np.asarray(y)
    m = np.asarray(m)
    print(f"Loaded data with shapes: {x.shape}, {y.shape}, {m.shape}")

    if cache:
        np.save(folder / "cache_x_dif", x)
        np.save(folder / "cache_y_dif", y)

    return x, y, m

def load_difference_vol(file_path, noise=False,  bar=True, shape=(256, 256, 25), **kwargs):

    file_path = pathlib.Path(file_path)
    files = list(file_path.glob('*.h5'))
    # print(file_path)
    x = np.zeros((len(files),) + shape)
    y = np.zeros((len(files),) + shape)
    for i, fname in tqdm(enumerate(files), disable=(not bar)):
        # for each volume generate a 3 channel image
        with h5py.File(fname, "r") as f:

            # print(f['concentration'].shape)
            num_slices = f['concentration'].shape[-1]
            pos = np.asarray(f['positive_grad'])
            neg = np.asarray(f['negative_grad'])
            conc = np.asarray(f['concentration'])
            # print(fname)
            # if noise:
            #     snr = 0.001
            #     # print(snr)
            #     pos += np.sqrt(np.random.normal(scale=snr, size=pos.shape)**2 + \
            #                    np.random.normal(scale=snr, size=pos.shape)**2)
            #     neg += np.sqrt(np.random.normal(scale=snr, size=neg.shape)**2 + \
            #                    np.random.normal(scale=snr, size=neg.shape)**2)


        if pos.shape[2] < 25:
            continue
        else:
            # divide by a constant
            dif_vol = (pos - neg)
            dif_vol = dif_vol[..., :25]
            conc = conc[..., :25]
            if np.any(np.logical_not(np.isfinite(dif_vol))):
                print("NaN")
            dif_vol[np.logical_not(np.isfinite(dif_vol))] = 0

            if np.any(np.logical_not(np.isfinite(conc))):
                print("NaN in conc")
            conc[np.logical_not(np.isfinite(conc))] = 0
            if noise:
                snr = 0.05
                dif_vol += np.random.normal(scale=snr, size=dif_vol.shape)
            # mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
            # mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
            # conc = (conc - np.mean(conc)) / np.std(conc)

            # dif = mVolneg - mVolpos


            # dif = (dif - np.mean(dif)) / np.std(dif)
            # print(dif_vol.shape)
            # print(conc.shape)
            x[i, ...] = dif_vol
            y[i, ...] = conc
    # print([i.shape for i in x])

    print(f"Loaded data with shapes: {x.shape}, {y.shape}")

    return x, y


def load_difference_vol_multi(file_path, noise=False,  bar=True, shape=(256, 25, 256), **kwargs):

    file_path = pathlib.Path(file_path)
    files = list(file_path.glob('*.h5'))
    # print(file_path)
    x = np.zeros((len(files),) + shape)
    y = np.zeros((len(files),) + shape)
    for i, fname in tqdm(enumerate(files), disable=(not bar)):
        # for each volume generate a 3 channel image
        with h5py.File(fname, "r") as f:

            # print(f['concentration'].shape)
            num_slices = f['concentration'].shape[-1]
            pos = np.asarray(f['positive_grad'])
            neg = np.asarray(f['negative_grad'])
            conc = np.asarray(f['concentration'])
            # print(fname)
            # if noise:
            #     snr = 0.001
            #     # print(snr)
            #     pos += np.sqrt(np.random.normal(scale=snr, size=pos.shape)**2 + \
            #                    np.random.normal(scale=snr, size=pos.shape)**2)
            #     neg += np.sqrt(np.random.normal(scale=snr, size=neg.shape)**2 + \
            #                    np.random.normal(scale=snr, size=neg.shape)**2)


        if pos.shape[2] < 25:
            continue
        else:
            # divide by a constant
            dif_vol = (pos - neg)
            dif_vol = dif_vol[..., :25]
            conc = conc[..., :25]
            if np.any(np.logical_not(np.isfinite(dif_vol))):
                print("NaN")
            dif_vol[np.logical_not(np.isfinite(dif_vol))] = 0

            if np.any(np.logical_not(np.isfinite(conc))):
                print("NaN in conc")
            conc[np.logical_not(np.isfinite(conc))] = 0
            if noise:
                snr = 0.05
                dif_vol += np.random.normal(scale=snr, size=dif_vol.shape)
            # mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
            # mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
            # conc = (conc - np.mean(conc)) / np.std(conc)

            # dif = mVolneg - mVolpos


            # dif = (dif - np.mean(dif)) / np.std(dif)
            # print(dif_vol.shape)
            # print(conc.shape)
            x[i, ...] = np.rot90(dif_vol, axes=(1, 2))
            y[i, ...] = np.rot90(conc, axes=(1, 2))
    # print([i.shape for i in x])

    print(f"Loaded data with shapes: {x.shape}, {y.shape}")

    return x, y

def load_vol(file_path, noise=False,  bar=True, **kwargs):

    file_path = pathlib.Path(file_path)
    files = list(file_path.glob('*.h5'))
    # print(file_path)
    x = []
    y = []
    pos_x = []
    neg_x = []
    for fname in tqdm(files, disable=(not bar)):
        # for each volume generate a 3 channel image
        with h5py.File(fname, "r") as f:

            # print(f['concentration'].shape)
            num_slices = f['concentration'].shape[-1]
            pos = normalize(np.asarray(f['positive_grad']))
            neg = normalize(np.asarray(f['negative_grad']))
            conc = np.asarray(f['concentration'])
            # print(fname)
            # if noise:
            #     snr = 0.001
            #     # print(snr)
            #     pos += np.sqrt(np.random.normal(scale=snr, size=pos.shape)**2 + \
            #                    np.random.normal(scale=snr, size=pos.shape)**2)
            #     neg += np.sqrt(np.random.normal(scale=snr, size=neg.shape)**2 + \
            #                    np.random.normal(scale=snr, size=neg.shape)**2)


        if pos.shape[2] < 25:
            continue
        else:
            # dif_vol = pos - neg
            dif_vol = normalize(neg - pos)
            dif_vol = dif_vol[..., :25]
            conc = conc[..., :25]
            if np.any(np.logical_not(np.isfinite(dif_vol))):
                print("NaN")
            dif_vol[np.logical_not(np.isfinite(dif_vol))] = 0
            if noise:
                snr = 0.05
                dif_vol += np.random.normal(scale=snr, size=dif_vol.shape)
            # mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
            # mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
            # conc = (conc - np.mean(conc)) / np.std(conc)

            # dif = mVolneg - mVolpos


            # dif = (dif - np.mean(dif)) / np.std(dif)
            # print(dif_vol.shape)
            # print(conc.shape)
            x.append(dif_vol)
            pos_x.append(pos[..., :25])
            neg_x.append(neg[..., :25])

            y.append(conc)
    # print([i.shape for i in x])
    x = np.asarray(x)
    y = np.asarray(y)

    pos_x = np.asarray(pos_x)
    neg_x = np.asarray(neg_x)

    print(f"Loaded data with shapes: {x.shape}, {y.shape}")

    return pos_x, neg_x, y


def load_difference_3d(file_path, noise=False,  bar=True, **kwargs):

    file_path = pathlib.Path(file_path)
    files = list(file_path.glob('*.h5'))
    # print(file_path)
    x = []
    y = []

    for fname in tqdm(files, disable=(not bar)):
        # for each volume generate a 3 channel image
        with h5py.File(fname, "r") as f:

            # print(f['concentration'].shape)
            num_slices = f['concentration'].shape[-1]
            pos = normalize(np.asarray(f['positive_grad']))
            neg = normalize(np.asarray(f['negative_grad']))
            conc = np.asarray(f['concentration'])
            # print(fname)
            # if noise:
            #     snr = 0.001
            #     # print(snr)
            #     pos += np.sqrt(np.random.normal(scale=snr, size=pos.shape)**2 + \
            #                    np.random.normal(scale=snr, size=pos.shape)**2)
            #     neg += np.sqrt(np.random.normal(scale=snr, size=neg.shape)**2 + \
            #                    np.random.normal(scale=snr, size=neg.shape)**2)


        if pos.shape[2] < 32:
            continue
        else:
            # dif_vol = pos - neg
            dif_vol = normalize(neg - pos)
            dif_vol = dif_vol[..., :32]
            conc = conc[..., :32]
            if np.any(np.logical_not(np.isfinite(dif_vol))):
                print("NaN")
            dif_vol[np.logical_not(np.isfinite(dif_vol))] = 0
            if noise:
                snr = 0.05
                dif_vol += np.random.normal(scale=snr, size=dif_vol.shape)
            # mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
            # mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
            # conc = (conc - np.mean(conc)) / np.std(conc)

            # dif = mVolneg - mVolpos


            # dif = (dif - np.mean(dif)) / np.std(dif)
            # print(dif_vol.shape)
            # print(conc.shape)
            x.append(dif_vol)
            y.append(conc)
    # print([i.shape for i in x])
    x = np.asarray(x)
    y = np.asarray(y)
    print(f"Loaded data with shapes: {x.shape}, {y.shape}")

    return x, y


def load_single_difference(file_path, dim):
    file_path = pathlib.Path(file_path)
    files = list(file_path.glob('*.h5'))

    single_file = np.random.choice(files)
    x = []
    y = []
    with h5py.File(single_file, "r") as f:
        num_slices = f['concentration'].shape[-1]
        pos = normalize(f['positive_grad'])
        neg = normalize(f['positive_grad'])
        snr = np.std(neg[..., 0])

        for j in range(1, num_slices-1):
            mVolpos = pos[..., j-1:j+2]
            mVolneg = neg[..., j-1:j+2]
            conc = f['concentration'][..., j]
            # conc = (conc - np.mean(conc)) / np.std(conc)
            dif = mVolneg  - mVolpos
            x.append(dif)
            y.append(conc)

    x = np.asarray(x)[..., np.newaxis]
    y = np.asarray(y)[..., np.newaxis]
    return x, y


def load_volume(file_path, dim, cache=False):

    if cache:
        folder = pathlib.Path(file_path)
        if (folder / "cache_x_vol.npy").is_file():
            x = np.load(folder / "cache_x_vol.npy")
            y = np.load(folder / "cache_y_vol.npy")
            return x, y

    file_path = pathlib.Path(file_path)
    files = list(file_path.glob('*.h5'))
    print(file_path)
    x = []
    y = []

    for fname in tqdm(files):
        # for each volume generate a 3 channel image
        with h5py.File(fname, "r") as f:
            num_slices = f['concentration'].shape[-1]
            for j in range(1, num_slices-1):
                mVolpos = f['positive_grad'][..., j-1:j+2]
                mVolneg = f['negative_grad'][..., j-1:j+2]
                conc = f['concentration'][..., j]

                mVolpos = (mVolpos - np.mean(mVolpos)) / np.std(mVolpos)
                mVolneg = (mVolneg - np.mean(mVolneg)) / np.std(mVolneg)
                # conc = (conc - np.mean(conc)) / np.std(conc)

                x.append(np.asarray([mVolneg, mVolpos]))
                y.append(conc)

    x = np.asarray(x)
    y = np.asarray(y)
    if cache:
        np.save(folder / "cache_x_vol", x)
        np.save(folder / "cache_y_vol", y)
    print(x.shape)
    print(y.shape)
    return x, y


def load_numpy(file_path, dim, cache=False):
    """Generates a numpy arrays from .h5 files contaning volume data. Internally
    uses the old generator, so it is slow the fist time
    """
    if cache:
        folder = pathlib.Path(file_path)
        if (folder / "cache_x.npy").is_file():
            x = np.load(folder / "cache_x.npy")
            y = np.load(folder / "cache_y.npy")
            return x, y

    gen = DataGenerator(file_path, batch_size=1, dim=dim)
    x = np.empty((len(gen), )+(dim))
    y = np.empty((len(gen), )+(dim[:-1]) + (1,) )
    # print(x.shape, y.shape)
    bar = progressbar(range(len(gen)))
    for i in bar():
        x[i, ...], y[i, ...] = gen[i]

    if cache:
        np.save(folder / "cache_x", x)
        np.save(folder / "cache_y", y)

    return (x, y)

class Loader():
    def __init__(self, folder_path):
        self.folder_path = pathlib.Path(folder_path)
        self.files = sorted(list(self.folder_path.glob('*.h5')))

    def __len__(self):
        return len(self.files)

    def _get_file(self, fname):
        with h5py.File(fname, "r") as f:
            pos = np.asarray(f['positive_grad'])
            neg = np.asarray(f['negative_grad'])
            conc = np.asarray(f['concentration'])

        dif_vol = pos - neg
        dif_vol = dif_vol[..., :25]
        conc = conc[..., :25]
        if np.any(np.logical_not(np.isfinite(dif_vol))):
            print("NaN")
        dif_vol[np.logical_not(np.isfinite(dif_vol))] = 0

        if np.any(np.logical_not(np.isfinite(conc))):
            print("NaN in conc")
        conc[np.logical_not(np.isfinite(conc))] = 0

        return dif_vol, conc

    def __getitem__(self, idx):
        fname = self.files[idx]
        return self._get_file(fname)

class LoaderWithDensity():
    def __init__(self, folder_path):
        self.folder_path = pathlib.Path(folder_path)
        self.files = sorted(list(self.folder_path.glob('*.h5')))

    def __len__(self):
        return len(self.files)

    def _get_file(self, fname):
        with h5py.File(fname, "r") as f:
            pos = np.asarray(f['positive_grad'])
            neg = np.asarray(f['negative_grad'])
            conc = np.asarray(f['concentration'])

        dif_vol = pos - neg
        dif_vol = dif_vol[..., :25]
        conc = conc[..., :25]
        if np.any(np.logical_not(np.isfinite(dif_vol))):
            print("NaN")
        dif_vol[np.logical_not(np.isfinite(dif_vol))] = 0

        if np.any(np.logical_not(np.isfinite(conc))):
            print("NaN in conc")
        conc[np.logical_not(np.isfinite(conc))] = 0

        return dif_vol, conc, pos, neg

    def __getitem__(self, idx):
        fname = self.files[idx]
        return self._get_file(fname)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    x, y = load_volume("./dataset/spio_data/train", (320, 320, 3))

    print(x.shape)
    print(y.shape)
    n = 7

    plt.subplot(121)
    plt.imshow(x[n, 0])
    plt.subplot(122)
    plt.imshow(x[n, 1])

    plt.figure()
    plt.imshow(y[n])
    plt.colorbar()
    plt.show()

def augment_and_noise(noise = True):

    def f1(image_spiomap, seed):
        image, spiomap = image_spiomap
        new_seed = tf.random.experimental.stateless_split(seed, num=1)[0, :]
        # Random flip
        spiomap = spiomap[None, ...]
        image = tf.image.stateless_random_flip_up_down(image, seed=new_seed)
        spiomap = tf.image.stateless_random_flip_up_down(spiomap, seed=new_seed)
        image = tf.image.stateless_random_flip_left_right(image, seed=new_seed)
        spiomap = tf.image.stateless_random_flip_left_right(spiomap, seed=new_seed)

        if noise:
            snr = tf.random.stateless_normal((1), mean=0.05, seed=new_seed+1, dtype='float64')
            image += tf.random.stateless_normal(image.shape, stddev= snr, seed=new_seed, dtype='float64')

        return image, tf.squeeze(spiomap)

    return f1