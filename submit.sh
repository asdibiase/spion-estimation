#!/bin/bash

# Verbosity level is set to 2 by default because it logged to a file

python train_rot.py \
                --datapath "./dataset/spio_big/" \
                --checkpoint "./weights/rot_prof_RMSprop_NMSE_DICE" \
                --model-type "2d+nBN" \
                --batchsize 64 \
                --activity-reg 1e-6 \
                --lr 1e-4 \
                --epochs 1000 \
                --loss "NMSE+DICE"
