
from numba import jit, njit, prange
import numpy as np
import matplotlib.pyplot as plt

import utils

# Concentration: 27.9 mg/ml
GAMMA = gamma = 42.58*10**6 # Hz/T

@njit()
def extSelect(Gz, w0, m, slice, FOV, dw, Z):
    """Outputs the detected image after elicitation.
    Args:
        Gz (scalar) gradient magnitude in T/m
        w0 (array-like) Fieldmap in T
        m (array-like) Volume contrast
        slice (scalar) [0..1] Portion of the Z axis to excite
        dw (scalar) Range of frequencies to excite (Hz)

    Returns:
        mrf: the image
        wrf: the excitation profile
        k: the gradient (in Hz)
    """

    # Excitation
    k = GAMMA*(Gz*Z + w0)
    kz = slice*GAMMA*Gz*FOV[2]
    # Soft selection of frequencies
    wrf = 0.5 + np.tanh((1 - (np.abs(k - kz)/(dw/2)) - 0.5)/0.5)/2
    # wrf = min(max(1 - (abs(k - kz)/(dw/2)), 0), 1)
    mrf = np.sum(wrf*m, axis=2)
    return mrf, wrf, k

@njit(parallel=True)
def extSelectVol(Gz, w0, m, FOV, N, Z, mode='tanh', alpha=1):
    """Outputs the detected volume after excitation, on 7T scanner
    Args:
        Gz (scalar) gradient magnitude in T/m
        w0 (array-like) Fieldmap in ppm
        m (array-like) Volume contrast
        N (int) number of slices
        mode (`tanh`, `lineal`) Type of excitation selection profile
        alpha (float) Slope of the excitation (only for `tanh`)
    Returns:
        mrf: the volume
    """

    slices = np.linspace(0, 1, N)
    Nx, Ny, Nz = m.shape
    mrf = np.zeros((Nx, Ny, N))
    dw = np.abs(GAMMA*Gz*FOV[2])/(N) # Hz # range of exited frequencies
    w0 = GAMMA*7*w0*10e-6 # Hz
    k = GAMMA*(Gz*Z) + w0

    for i in prange(0, N):
        kz = slices[i]*GAMMA*Gz*FOV[2]
        # m1, _, _ = extSelect(Gz, w0, m, s, FOV, dw, Z)


        if mode == 'lineal':
            wrf = np.minimum(np.maximum(1-alpha*(np.abs(k - kz)/(dw/2)), 0), 1)
        elif mode == 'tanh':
            wrf = 0.5 + 0.5*np.tanh(alpha*(1 - 2*(np.abs(k - kz)/(dw/2))))
        else:
            raise ValueError("mode must be `tanh` or `lineal`.")

        mrf[:, :, i] = np.sum(wrf*m, 2)
        # mrf[:, :, i] = utils.normalize(np.sum(wrf*m, 2))
        # plt.plot(wrf[Nx//2, Ny//2, :])
        # plt.show()
    return mrf

def my_dipole_kernel(N):
    kxx = np.linspace(-N[0]/2, N[0]/2-1, N[0])
    kyy = np.linspace(-N[1]/2, N[1]/2-1, N[1])
    kzz = np.linspace(-N[2]/2, N[2]/2-1, N[2])

    kx, ky, kz = np.meshgrid(kxx, kyy, kzz)

    d = 1/3 - kz**2 / (kx**2 + ky**2 + kz**2 + 1e-12)

    return np.fft.fftshift(d)

def dipole_kernel(matrix_size, voxel_size, B0_dir, mode):
    """Calculates a dipole kernel
    Args:
        matrix_size - array size
        spatial_res -
        mode - 0 for the continuous kernel proposed by Salomir, et al. 2003.
                1 for the discrete kernel proposed by Milovic, et al. 2017.
                2 for the Integrated Green function proposed by Jenkinson, et al. 2004
        B0_dir - main field direction, e.g. [0, 0, 1]

    Returns:
        kernel - dipole kernel in the frequency space

    Created by Carlos Milovic, 30.03.2017
    Last modified by Julio Acosta-Cabronero, 26.05.2017
    Ported to Python by Alberto Di Biase, 31.07.2020
    """
    kxx = np.arange(-matrix_size[1]/2, matrix_size[1]/2 - 1)
    kyy = np.arange(-matrix_size[0]/2, matrix_size[0]/2 - 1)
    kzz = np.arange(-matrix_size[2]/2, matrix_size[2]/2 - 1)

    kxx = np.arange(-matrix_size[1]/2, matrix_size[1]/2)
    kyy = np.arange(-matrix_size[0]/2, matrix_size[0]/2)
    kzz = np.arange(-matrix_size[2]/2, matrix_size[2]/2)

    ky, kx, kz = np.meshgrid(kyy, kxx, kzz)
    matrix_size = np.asarray(matrix_size)
    voxel_size = np.asarray(voxel_size)
    eps = np.finfo(np.float64).resolution

    # Continuous kernel
    if mode == 0:
        kx = (kx / np.max(np.abs(kx))) / voxel_size[0]
        ky = (ky / np.max(np.abs(ky))) / voxel_size[1]
        kz = (kz / np.max(np.abs(kz))) / voxel_size[2]

        k2 = kx**2 + ky**2 + kz**2
        k2[k2==0] = eps

        # R_tot = eye(3);
        # kernel = fftshift( 1/3 - (kx * R_tot(3,1) + ky * R_tot(3,2) + kz * R_tot(3,3)).^2 ./ (k2 + eps) );

        # JAC
        kernel = np.fft.ifftshift( 1/3.0 - (kx*B0_dir[0] + ky*B0_dir[1] + kz*B0_dir[2])**2 / k2 )
        kernel[0, 0, 0] = 0.0
        return kernel

    elif mode == 1: # Discrete kernel

        FOV = matrix_size*voxel_size
        center = 1+matrix_size/2
        kx = np.arange(1, matrix_size[0]+1)
        ky = np.arange(1, matrix_size[1]+1)
        kz = np.arange(1, matrix_size[2]+1)

        kx = kx - center[0]
        ky = ky - center[1]
        kz = kz - center[2]

        delta_kx = 1/FOV[0]
        delta_ky = 1/FOV[1]
        delta_kz = 1/FOV[2]

        kx = kx * delta_kx
        ky = ky * delta_ky
        kz = kz * delta_kz

        length = lambda x : np.max(x.shape)
        kx = np.reshape(kx, (length(kx), 1, 1))
        ky = np.reshape(ky, (1, length(ky), 1))
        kz = np.reshape(kz, (1, 1, length(kz)))

        kx = np.tile(kx, (1, matrix_size[1], matrix_size[2]) )
        ky = np.tile(ky, (matrix_size[0], 1, matrix_size[2]) )
        kz = np.tile(kz, (matrix_size[0], matrix_size[1], 1) )

        k2 = -3 + np.cos(2*np.pi*kx) + np.cos(2*np.pi*ky) + np.cos(2*np.pi*kz)
        k2[k2==0] = eps
        kernel = 1/3.0 - (-1 + np.cos(2*np.pi*kz)) / k2



        DC = (kx==0) & (ky==0) & (kz==0)
        kernel[DC==1] = 0
        kernel = np.fft.fftshift(kernel)

        return kernel

    elif mode == 2:
        raise NotImplementedError

def get_fieldmap(object, concen, matrix_size=[256,256,256], voxel_size=[0.195,0.195,0.195], B_dir=[0, 0, 1]):
    """Calculates the fieldmap for a given shape with uniform concentration.
    Args:
        Object (array): the shape matrix
        concen (float): The concentraion of SPIO, 1 is the 27.9um/uL
        matrix_size: default is [256,256,256]
        voxel_size:default is [0.195,0.195,0.195]
        B_dir: default is [0,0,1] z direction
    Returns:
        Field Map( unit: Tesla)
        Author: Alberto Di Biase (2020/01/22), adapted from matlab code by
        Shuang Liu
    """

    # for imagespace:
    # D = my_dipole_kernel(matrix_size)
    # D = D*1e-2
    # for kspace
    D = dipole_kernel(matrix_size, voxel_size, B_dir, mode=0)
    # D = my_dipole_kernel(matrix_size)
    Total_Moment = 2.08 * 10**(-5) # unit:A/mm (2.08e-5  A m^2/10 mm^3 -> 2.08e1/10 A mm2/mm^3)
    # momPerVoxel = 2.08*10
    Mu0 = 4*np.pi*1e-7  # T mm/A original value is 4pi*1e-7 T m/A

    Dconv = lambda input_x: np.real(np.fft.ifftn((D*np.fft.fftn(input_x)) ))
    field_map = Dconv(object) * concen #*Mu0*Total_Moment*concen
    return field_map

class Simulator():
    def __init__(self, matrix_size=[256,256,256], voxel_size=[0.195,0.195,0.195], Z=None, mode='tanh', alpha=1.0):
        self.matrix_size = matrix_size
        self.voxel_size = voxel_size
        self.B_dir = [0, 0, 1]
        self.FOV = matrix_size * voxel_size
        self.Z = Z
        self.mode = mode
        self.alpha = alpha

        self.kernel = dipole_kernel(matrix_size, voxel_size, [0, 0, 1], mode=0)
        self.Dconv = lambda input_x: np.real(np.fft.ifftn((self.kernel*np.fft.fftn(input_x)) ))
        # self.Dconv = lambda input_x: np.fft.ifftn((self.kernel*np.fft.fftn(input_x)))

    def get_fieldmap(self, object):
        return self.Dconv(object)

    def get_extSelectVol(self, Gz, fieldmap, m, N):
        return extSelectVol(Gz, fieldmap, m, self.FOV, N, self.Z, self.mode, self.alpha)


if __name__ == "__main__":

    from skimage.util import montage
    from utils import cylinder, map

    FOV = 1e-3 * np.array([50, 50, 25]) # m
    N = np.asarray([256, 256, 256])
    x = np.linspace(0, FOV[0], N[0])
    y = np.linspace(0, FOV[1], N[1])
    z = np.linspace(0, FOV[2], N[2])
    X, Y, Z = np.meshgrid(x, y, z)

    Gz = 0.09*440e-3 # T/m

    sim = Simulator(N, (0.195, 0.195, 1.0), Z)

    object = 1.0*cylinder(40e-3, 16e-3, 1e-3*np.array([25, 25, 12.5]), X, Y, Z)
    blob = 1.0*cylinder(4e-3, 1.4e-3, 1e-3*np.array([25, 25, 12.5]), X, Y, Z)

    object[blob > 0] = 0.0
    blob = map(blob, out_max=1000.0, out_min=0.0)

    # fieldmap = 1*np.exp(((X-FOV[0]/2)/4)**2 + \
    #                      ((Y-FOV[1]/2)/4)**2 + \
    #                      ((Z-FOV[2]/2)/4)**2 ) + \
    #     5*np.exp(((X-FOV[0]/2)/8)**2 + ((Y-FOV[1]/2)/8)**2 + ((Z-FOV[2]/2)/8)**2)
    fieldmap = sim.get_fieldmap(blob)
    # Excitation
    mVol = sim.get_extSelectVol(Gz, fieldmap, object, 25)
    dw = np.abs(GAMMA*Gz*FOV[2])/(25)
    mrf, wrf, k = extSelect(Gz, fieldmap, object, 0.5, FOV, dw, Z)



    plt.figure(1)
    plt.plot(np.real(fieldmap[:, 128, 128]))
    plt.plot(np.imag(fieldmap[:, 128, 128]))
    plt.title("Fieldmap real imag")

    plt.figure(2)
    plt.plot(np.abs(fieldmap[:, 128, 128]))
    plt.plot(np.angle(fieldmap[:, 128, 128]))
    plt.title("Fieldmap abs arg")


    plt.figure(3)
    plt.imshow(montage(object.T), cmap='gray')
    plt.title("object")
    plt.colorbar()

    plt.figure(4)
    plt.imshow(montage(mVol.T), cmap='gray')
    plt.title("obs")

    plt.figure(5)
    plt.imshow(montage(blob.T), cmap='gray')
    plt.title("blob")
    plt.colorbar()

    plt.show()
