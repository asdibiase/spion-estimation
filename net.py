import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras import Model
from tensorflow.keras.utils import plot_model


def build_model(shape, res_bn=False, name='unet', transposeConv=False, out_channels=1, skip_connection=False, dilation=False, batchnorm=True, output_reg=0, l2_penalty=0):
    def block_down(x, filters, kernel=(3, 3), dilation_rate=(1, 1)):
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          dilation_rate=dilation_rate,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          dilation_rate=dilation_rate,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        x = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='same')(x)
        return x

    def block_up_upsampling(x, filters, kernel=(3, 3)):
        x = layers.UpSampling2D(size=(2, 2))(x)
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        # x = layers.UpSampling2D(size=(2, 2))(x)
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        return x


    def block_up_transpose(x, filters, kernel=(3, 3)):
        x = layers.Conv2DTranspose(filters=filters, kernel_size=kernel,
                                   strides=(2, 2),
                                   padding='same',
                                   kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        # x = layers.UpSampling2D(size=(2, 2))(x)
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        return x


    def res_bottleneck(x_in, num_layers=4, filters=128, kernel=(3, 3)):
        for i in range(num_layers):
            x = layers.Conv2D(filters=filters, kernel_size=kernel,
                              activation='relu',
                              padding='same',
                              name='bottleneck_{}'.format(i),
                              kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x_in)
            x_in = layers.Add()([x_in, x])
        return x


    block_up = block_up_transpose if transposeConv else block_up_upsampling
    inputx = layers.Input(shape)
    # inputx = layers.Conv2D(64, kernel_size=(3, 3), padding='same')(inputx)
    if dilation:
        d_64 = block_down(inputx, 64, kernel=(3, 3), dilation_rate=(4, 4))
        d_128 = block_down(d_64, 128, kernel=(3, 3), dilation_rate=(2, 2))
    else:
        d_64 = block_down(inputx, 64, kernel=(3, 3))
        d_128 = block_down(d_64, 128, kernel=(3, 3))

    d_256 = block_down(d_128, 256)
    # print(d_256.shape)
    d_512 = block_down(d_256, 512)
    # print(d_512.shape)

    if res_bn:
        bottleneck = res_bottleneck(d_512, filters=512)
        # print(bottleneck.shape)
    else:
        bottleneck = layers.Conv2D(filters=1024, kernel_size=(3, 3), activation='relu', padding='same', name='bottleneck')(d_512)

    # print(bottleneck.shape)
    u_512 = block_up(bottleneck, 512)
    if skip_connection: u_512 = layers.Concatenate(name='concat_512_256')([u_512, d_256])
    u_256 = block_up(u_512, 256)
    if skip_connection: u_256 = layers.Concatenate(name='concat_256_128')([u_256, d_128])
    u_128 = block_up(u_256, 128, kernel=(3, 3))
    if skip_connection: u_128 = layers.Concatenate(name='concat_128_64')([u_128, d_64])
    u_64 = block_up(u_128, 64)
    # u_64 = block_up(u_64, 64, kernel=(15, 15))

    x = layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same')(u_64)
    # cons = layers.Add()([x, inputx])
    # x = layers.UpSampling2D(size=(2, 2))(x)
    # output has only one channel
    # cons = layers.Conv2D(filters=out_channels, kernel_size=(1, 1), padding='same', activity_regularizer=tf.keras.regularizers.l2(1e-4))(x)
    cons = layers.Conv2D(filters=out_channels, kernel_size=(1, 1), padding='same', kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
    cons = layers.ReLU(name='', activity_regularizer=tf.keras.regularizers.l1(output_reg))(cons)
    return Model(inputs=inputx, outputs=cons, name=name)


def build_model2d_rot(shape, res_bn=False, name='unet', transposeConv=False, out_channels=1, skip_connection=False, dilation=False, batchnorm=True, output_reg=0, l2_penalty=0):

    def block_down(x, filters, kernel=(3, 3), dilation_rate=(1, 1)):
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          dilation_rate=dilation_rate,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          dilation_rate=dilation_rate,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        x = layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='same')(x)
        return x

    def block_up_upsampling(x, filters, kernel=(3, 3)):
        x = layers.UpSampling2D(size=(2, 2))(x)
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        # x = layers.UpSampling2D(size=(2, 2))(x)
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        return x


    def block_up_transpose(x, filters, kernel=(3, 3)):
        x = layers.Conv2DTranspose(filters=filters, kernel_size=kernel,
                                   strides=(2, 2),
                                   padding='same',
                                   kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        # x = layers.UpSampling2D(size=(2, 2))(x)
        x = layers.Conv2D(filters=filters, kernel_size=kernel,
                          padding='same',
                          kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        return x


    def res_bottleneck(x_in, num_layers=4, filters=128, kernel=(3, 3)):
        for i in range(num_layers):
            x = layers.Conv2D(filters=filters, kernel_size=kernel,
                              activation='relu',
                              padding='same',
                              name='bottleneck_{}'.format(i),
                              kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x_in)
            x_in = layers.Add()([x_in, x])
        return x


    block_up = block_up_transpose if transposeConv else block_up_upsampling
    inputx = layers.Input(shape)
    x = layers.ZeroPadding2D(padding=(0, 3))(inputx)
    print(x.shape)
    # inputx = layers.Conv2D(64, kernel_size=(3, 3), padding='same')(inputx)
    if dilation:
        d_64 = block_down(x, 64*4, kernel=(3, 3), dilation_rate=(4, 4))
        d_128 = block_down(d_64, 128*4, kernel=(3, 3), dilation_rate=(2, 2))
    else:
        d_64 = block_down(x, 64*4, kernel=(3, 3))
        d_128 = block_down(d_64, 128*4, kernel=(3, 3))

    d_256 = block_down(d_128, 256*4)
    # print(d_256.shape)
    d_512 = block_down(d_256, 512*4)
    # print(d_512.shape)

    if res_bn:
        bottleneck = res_bottleneck(d_512, filters=512*4)
        # print(bottleneck.shape)
    else:
        bottleneck = layers.Conv2D(filters=1024*4, kernel_size=(3, 3), activation='relu', padding='same', name='bottleneck')(d_512)

    # print(bottleneck.shape)
    u_512 = block_up(bottleneck, 512*4)
    if skip_connection: u_512 = layers.Concatenate(name='concat_512_256')([u_512, d_256])
    u_256 = block_up(u_512, 256*4)
    if skip_connection: u_256 = layers.Concatenate(name='concat_256_128')([u_256, d_128])
    u_128 = block_up(u_256, 128*4, kernel=(3, 3))
    if skip_connection: u_128 = layers.Concatenate(name='concat_128_64')([u_128, d_64])
    u_64 = block_up(u_128, 64*4)
    # u_64 = block_up(u_64, 64, kernel=(15, 15))

    x = layers.Conv2D(filters=64*4, kernel_size=(3, 3), activation='relu', padding='same', kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(u_64)
    x = layers.MaxPool2D(pool_size=(1, 8), strides=(1, 1))(x)
    cons = layers.Conv2D(filters=out_channels, kernel_size=(1, 1), padding='same', kernel_regularizer=tf.keras.regularizers.l2(l2_penalty))(x)
    cons = layers.ReLU(name='', activity_regularizer=tf.keras.regularizers.l1(output_reg))(cons)
    print(cons.shape, inputx.shape)
    return Model(inputs=inputx, outputs=cons, name=name)


def build_model3d(shape, res_bn=False, name='unet', transposeConv=False, dilation=False, batch_norm=True, output_reg=0):

    def block_down(x, filters, dilation_rate=(1, 1, 1)):
        x = layers.Conv3D(filters=filters, kernel_size=(3, 3, 3),
                          dilation_rate=dilation_rate,
                          padding='same',
                          name=f"conv3d_down_1_{filters}")(x)
                        #   kernel_regularizer=tf.keras.regularizers.l2(1e-4))(x)
        if batch_norm: x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        x = layers.Conv3D(filters=filters, kernel_size=(3, 3, 3),
                          dilation_rate=dilation_rate,
                          padding='same',
                          name=f"conv3d_down_2_{filters}")(x)
                        #   kernel_regularizer=tf.keras.regularizers.l2(1e-4))(x)
        if batch_norm: x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        x = layers.MaxPool3D(pool_size=(2, 2, 2), strides=(2, 2, 2), padding='same')(x)
        return x

    def block_up_upsampling(x, filters):
        x = layers.UpSampling3D(size=(2, 2, 2))(x)
        x = layers.Conv3D(filters=filters, kernel_size=(3, 3, 3),
                          padding='same',
                          name=f"conv3d_up_1_{filters}")(x)
                        #   kernel_regularizer=tf.keras.regularizers.l2(1e-4))(x)
        if batch_norm: x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        # x = layers.UpSampling2D(size=(2, 2))(x)
        x = layers.Conv3D(filters=filters, kernel_size=(3, 3, 3),
                          padding='same',
                          name=f"conv3d_up_2_{filters}")(x)
                        #   kernel_regularizer=tf.keras.regularizers.l2(1e-4))(x)
        if batch_norm: x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        return x


    def block_up_transpose(x, filters):
        x = layers.Conv3DTranspose(filters=filters, kernel_size=(3, 3, 3),
                                   strides=(2, 2, 2),
                                   padding='same',
                                   name=f"transconv3d_up_1_{filters}")(x)
                                #    kernel_regularizer=tf.keras.regularizers.l2(1e-4))(x)
        if batch_norm: x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        # x = layers.UpSampling2D(size=(2, 2))(x)
        x = layers.Conv3D(filters=filters, kernel_size=(3, 3, 3),
                          activation='relu',
                          padding='same',
                          name=f"transconv3d_up_2_{filters}")(x)

                        #   kernel_regularizer=tf.keras.regularizers.l2(1e-4))(x)
        if batch_norm: x = layers.BatchNormalization()(x)
        x = layers.ReLU()(x)
        return x


    def res_bottleneck(x_in, num_layers=4, filters=128):
        for i in range(num_layers):
            x = layers.Conv3D(filters=filters, kernel_size=(3, 3, 3),
                              activation='relu',
                              padding='same',
                              name='bottleneck_{}'.format(i))(x_in)
                            #   kernel_regularizer=tf.keras.regularizers.l2(1e-4))(x_in)
            x_in = layers.Add()([x_in, x])
        return x


    block_up = block_up_transpose if transposeConv else block_up_upsampling
    inputx = layers.Input(shape)
    if dilation:
        d_64 = block_down(inputx, 64, dilation_rate=(4, 4, 4))
        d_128 = block_down(d_64, 128, dilation_rate=(2, 2, 2))
    else:
        d_64 = block_down(inputx, 64)
        d_128 = block_down(d_64, 128)

    d_256 = block_down(d_128, 256)
    print(d_256.shape)
    d_512 = block_down(d_256, 512)
    print(d_512.shape)

    if res_bn:
        bottleneck = res_bottleneck(d_512, filters=512)
        # print(bottleneck.shape)
    else:
        bottleneck = layers.Conv3D(filters=1024, kernel_size=(3, 3, 3), activation='relu', padding='same', name='bottleneck')(d_512)

    # print(bottleneck.shape)
    u_512 = block_up(bottleneck, 512)
    # u_512 = layers.Concatenate(name='concat_512_256')([u_512, d_256])
    u_256 = block_up(u_512, 256)
    # u_256 = layers.Concatenate(name='concat_256_128')([u_256, d_128])
    u_128 = block_up(u_256, 128)
    # u_64 = layers.Concatenate(name='concat_128_64')([u_128, d_64])
    # u_64 = block_up(u_64, 64)
    u_64 = block_up(u_128, 64)

    x = layers.Conv3D(filters=64, kernel_size=(3, 3, 3), activation='relu', padding='same')(u_64)
    # x = layers.UpSampling2D(size=(2, 2))(x)
    # output has only one channel
    x = layers.AveragePooling3D(pool_size=(1, 1, 8), strides=(1, 1, 1))(x)
    cons = layers.Conv3D(filters=1, kernel_size=(1, 1, 1), padding='same', name="final_conv3d")(x)
    cons = layers.ReLU(name='output', activity_regularizer=tf.keras.regularizers.l1(output_reg))(cons)
    return Model(inputs=inputx, outputs=cons, name=name)

def IoULoss(y_true, y_pred):
    # y_true, y_pred = tf.abs(y_true) > 0, tf.abs(y_pred) > 0
    intersection = tf.reduce_sum(tf.abs(y_true * y_pred))
    union = tf.reduce_sum(tf.abs(y_true) + tf.abs(y_pred)) - intersection
    # print(intersection, union)
    return 1.0 - (intersection / (union + 1e-12))

def DICE(batch_size, is3d=False):
    def dice_loss(y_true, y_pred):
        if is3d:
            nom = 2*tf.reduce_sum(y_true * y_pred, axis=[1, 2, 3, 4])
            den = tf.reduce_sum(tf.square(y_true), axis=[1, 2, 3, 4]) + \
                  tf.reduce_sum(tf.square(y_pred), axis=[1, 2, 3, 4])
        else:
            nom = 2*tf.reduce_sum(y_true * y_pred, axis=[1, 2, 3])
            den = tf.reduce_sum(tf.square(y_true), axis=[1, 2, 3]) + \
                  tf.reduce_sum(tf.square(y_pred), axis=[1, 2, 3])
        return tf.nn.compute_average_loss(1 - (nom + 1) / (den + 1), global_batch_size=batch_size)
    return dice_loss

def NMAE(batch_size, is3d=False):
    def nmae_loss(y_true, y_pred):
        if is3d:
            diff = tf.reduce_sum(tf.abs(y_true - y_pred), axis=[1, 2, 3, 4])
            norm = tf.reduce_sum(tf.abs(y_true), axis=[1, 2, 3, 4])
        else:
            diff = tf.reduce_sum(tf.abs(y_true - y_pred), axis=[1, 2, 3])
            norm = tf.reduce_sum(tf.abs(y_true), axis=[1, 2, 3])
        # print(norm, diff, nmae)
        # return tf.reduce_sum(nmae) / batch_size
        return tf.nn.compute_average_loss(diff / (norm + 1e-7), global_batch_size=batch_size)
    return nmae_loss

def NMSE(batch_size, is3d=False):
    def nmse_loss(y_true, y_pred):
        if is3d:
            diff = tf.reduce_sum(tf.square(y_true - y_pred), axis=[1, 2, 3, 4])
            norm = tf.reduce_sum(tf.square(y_true), axis=[1, 2, 3, 4])
        else:
            diff = tf.reduce_sum(tf.square(y_true - y_pred), axis=[1, 2, 3])
            norm = tf.reduce_sum(tf.square(y_true), axis=[1, 2, 3])
        # print(norm, diff, nmse)
        # return tf.reduce_sum(nmse) / batch_size
        return tf.nn.compute_average_loss(diff / (norm + 1e-7), global_batch_size=batch_size)
    return nmse_loss

def L1_REG(batch_size, is3d=False):
    def l1_regularization(y_true, y_pred):
        if is3d:
            penalty = tf.reduce_sum(tf.abs(y_pred), axis=[1, 2, 3, 4])
        else:
            penalty = tf.reduce_sum(tf.abs(y_pred), axis=[1, 2, 3])
        return tf.nn.compute_average_loss(penalty, global_batch_size=batch_size)
    return l1_regularization

if __name__ == "__main__":

    model = build_model2d_rot((256, 25, 256), res_bn=True, name="res_unet", transposeConv=True,
        out_channels=256, skip_connection=True, batchnorm=False, output_reg=1e-6, l2_penalty=0)
    model.summary()
    plot_model(model, "./2d+_rot.png", show_shapes=True)
