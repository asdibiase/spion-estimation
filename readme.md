
# Intensity-based Deep Learning for SPION concentration estimation in MR imaging

This repository contains the code necessary to repleacate the results of my
master thesis and *paper* to be publish.

The objective of the work was to estimate the concentration of Superparamagnetic
Iron Oxide Nanoparticles (SPION) from a pair of dual polarity gradient image in
MRI.

![Overview of the work](overview.png)

This repo uses git LFS make sure to install it before cloning.

The `environment.yml` file contains a list of depenadancies for the project.
Run `conda env create -f environment.yml` and  then `conda activate SPION`

## Data generation

To replicate the results first is necessary to generate the dataset. This is
done by running the scripts `submit_test.sh`, `submit_train_1.sh` and
`submit_train_2.sh`. Variables names may differ from the nomenclature used in
the paper.

## Training

Training is done by running the script `submit.sh`.

## Inference

Weights for the present network are stored [here](https://drive.google.com/file/d/1KSKv8Mr-flcWCsi12wHlGD0PUqSliVzi). The script `eval.py` can be
used to make inference on the network on
the real images stored in `dataset/invivo/`.
