#%%
from datetime import datetime
import argparse
import os
import pathlib
from itertools import permutations

import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.optimizers import Adam, RMSprop
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping, LambdaCallback, CSVLogger, LearningRateScheduler

import net, getdata

AUTOTUNE = tf.data.experimental.AUTOTUNE
losses = ['MSE', 'MAE', 'NMSE', 'NMAE', 'DICE']

def load_data(path, batchsize, is3d=False):
    print("loading train data...", flush=True)
    train_x, train_y = getdata.load_difference_vol(path / 'train', cache=False, noise=False, bar=False)
    # train_x = (train_x - getdata.mean_x) / getdata.std_x
    # train_y = (train_y - getdata.mean_y) / getdata.std_y
    print("train data loaded", flush=True)

    print("loading test data...", flush=True)
    val_x, val_y = getdata.load_difference_vol(path / 'test', cache=False, noise=False, bar=False)
    # val_x = (val_x - getdata.mean_x) / getdata.std_x
    # val_y = (val_y - getdata.mean_y) / getdata.std_y
    print("test data loaded", flush=True)

    if is3d:
        train_x, train_y = (np.expand_dims(train_x, -1), np.expand_dims(train_y, -1))
        val_x, val_y = (np.expand_dims(val_x, -1), np.expand_dims(val_y, -1))

    train_data = tf.data.Dataset.from_tensor_slices(
        (train_x, train_y)
        ).shuffle(100, reshuffle_each_iteration=True).batch(batchsize).prefetch(AUTOTUNE)

    val_data = tf.data.Dataset.from_tensor_slices(
        (val_x, val_y)
        ).batch(batchsize).prefetch(AUTOTUNE)

    return train_data, val_data

def choose_loss(option, batchsize, is3d):
    if option == 'MSE':
        return 'mse'
    elif option == 'NMSE':
        return net.NMSE(batchsize, is3d)
    elif option == 'MAE':
        return 'mae'
    elif option == 'NMAE':
        return net.NMAE(batchsize, is3d)
    elif option == 'DICE':
        return net.DICE(batchsize, is3d)

def get_loss(loss_name, batchsize, is3d, loss_weight):
    if '+' in loss_name:
        loss_name = loss_name.split('+')
        losses = [choose_loss(l, batchsize, is3d) for l in loss_name]
        loss = lambda y_true, y_pred : losses[0](y_true, y_pred) + \
                                       loss_weight*losses[1](y_true, y_pred)
        return loss
    else:
        return choose_loss(loss_name, batchsize, is3d)

def get_model(model_type, loss, batchsize, lr, activity_reg, weight_penalty, is3d):

    if model_type == '2d+':
        model = net.build_model((256, 256, 25), res_bn=True, name="res_unet", transposeConv=True,
        out_channels=25, skip_connection=True, output_reg=activity_reg, l2_penalty=weight_penalty)
    elif model_type == '2d+nBN':
        model = net.build_model((256, 256, 25), res_bn=True, name="res_unet", transposeConv=True,
        out_channels=25, skip_connection=True, batchnorm=False, output_reg=activity_reg, l2_penalty=weight_penalty)
    elif model_type == '2d+d':
        model = net.build_model((256, 256, 25), res_bn=True, name="res_dilatation_unet", transposeConv=True, out_channels=25, skip_connection=True, dilation=True, output_reg=activity_reg, l2_penalty=weight_penalty)
    elif model_type == '3d':
        model = net.build_model3d((256, 256, 25, 1), res_bn=True, name="res_3d_unet", transposeConv=False, output_reg=activity_reg, batch_norm=False)
    elif model_type == '3dd':
        model = net.build_model3d((256, 256, 25, 1), res_bn=True, name="res_3d_dilatation_unet", transposeConv=False, dilation=True, output_reg=activity_reg)
    elif model_type == '3ds':
        model = net.build_model3d_depthwise((256, 256, 25, 1), res_bn=True, name="res_3d_sep_unet", transposeConv=False, dilation=False)
    elif model_type == '3dds':
        model = net.build_model3d_depthwise((256, 256, 25, 1), res_bn=True, name="res_3d_sep_dilatation_unet", transposeConv=False, dilation=True)
    elif model_type == '3dsep':
        model = net.build_model3d_sep((256, 256, 25, 1), res_bn=True, name="res_3d_sep_simple_unet", transposeConv=False, dilation=True)
    elif model_type == '3dsmall':
        model = net.build_model3d_small((256, 256, 25, 1))
    elif model_type == '3dsmall_no_BN':
        model = net.build_model3d_small_no_BN((256, 256, 25, 1))
    elif model_type == '3dsmall_BNa':
        model = net.build_model3d_small_BNa((256, 256, 25, 1))


    opt = RMSprop(learning_rate=lr)
    model.compile(optimizer=opt, loss=loss, metrics=['mse', 'mae', net.NMSE(batchsize, is3d), net.NMAE(batchsize, is3d), net.L1_REG(batchsize, is3d), net.DICE(batchsize, is3d)])

    return model

def make_or_restore_model(check_point_dir, model_type, loss, batchsize, lr, is3d, activity_reg,weight_penalty, model):
    latest = model
    if model is None:
        try:
            latest = sorted(check_point_dir.glob('*.h5'), key=os.path.getmtime)[-1]
        except IndexError:
            latest = []
    if latest:
        print("Loading from", latest, flush=True)
        model = tf.keras.models.load_model(latest.as_posix(), compile=False)
        opt = Adam(learning_rate=lr)
        # Add regularization to the output
        model.layers[-1].activity_regularizer=tf.keras.regularizers.l1(activity_reg)
        model.compile(optimizer=opt, loss=loss, metrics=['mse', 'mae', net.NMSE(batchsize, is3d), net.NMAE(batchsize, is3d), net.L1_REG(batchsize, is3d), net.DICE(batchsize, is3d)])
        return model

    return get_model(model_type, loss, batchsize, lr, activity_reg, weight_penalty, is3d)

def schedule(epoch, lr):
    return lr
    # if epoch < 50:
    #     return lr
    # return max(lr * 0.9, 1e-6)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('--datapath',
                        help="Path as the data",
                        required=True,
                        type=pathlib.Path)
    parser.add_argument('--checkpoint',
                        help="Path to save models",
                        required=True,
                        type=pathlib.Path)
    parser.add_argument('--model-type', required=True,
                        help="Specifies the model type",
                        choices=['2d+', '2d+nBN', '2d+d','3d', '3dd','3ds', '3dds', '3dsep', '3dsmall','3dsmall_no_BN', '3dsmall_BNa'],)
    parser.add_argument('--model', type=pathlib.Path,
                        help="path to the model to load (only if --model-type is not set")
    parser.add_argument('--loss', default='MSE',
                        choices=[*losses, *[f"{l1}+{l2}" for l1, l2 in permutations(losses, 2)]],
                        help="Loss function used")
    parser.add_argument('--batchsize',
                        default=32,
                        type=int)
    parser.add_argument('--start-epoch',
                        default=0,
                        type=int)
    parser.add_argument('--activity-reg', type=float, default=0.0,
                        help="Activity regularizer parameter for l1 regularization of the output")
    parser.add_argument('--weight-decay', type=float, default=0.0,
                        help="l2 penalty for weights decay (l2 regularization")
    parser.add_argument('--loss-weight', type=float, default=1.0,
                        help="Weight of the second loss in case multiple cases multiple losses are choosen")
    parser.add_argument('--lr', default=1e-3, type=float)
    parser.add_argument('--epochs',
                        default=500,
                        type=int)
    parser.add_argument('--verbose', default=2, type=int,
                        help="Verbosity level of training")

    args = parser.parse_args()
    print(args)
    args.checkpoint.mkdir(exist_ok=True, parents=True)
    is3d = False
    if '3d' in args.model_type:
        is3d = True

    loss = get_loss(args.loss, args.batchsize, is3d, args.loss_weight)
    print("Loading data", flush=True)
    train_data, val_data = load_data(args.datapath, args.batchsize, is3d)

    num_gpus = len(tf.config.list_physical_devices('GPU'))
    if num_gpus == 0:
        print("No GPU detected", flush=True)
        model = make_or_restore_model(args.checkpoint, args.model_type, loss, args.batchsize, args.lr, is3d, args.activity_reg, args.weight_decay, args.model)
    elif num_gpus == 1:
        print("One GPU detected", flush=True)
        model = make_or_restore_model(args.checkpoint, args.model_type, loss, args.batchsize, args.lr, is3d, args.activity_reg, args.weight_decay, args.model)
    elif num_gpus > 1:
        strategy = tf.distribute.MirroredStrategy()
        print('Number of devices: {}'.format(strategy.num_replicas_in_sync), flush=True)
        with strategy.scope():
            model = make_or_restore_model(args.checkpoint, args.model_type, loss, args.batchsize, args.lr, is3d, args.activity_reg, args.weight_decay, args.model)

    model.summary()
    # tf.keras.utils.plot_model(model, to_file="pics/3d_sep.png", show_shapes=True)

    logdir = './logs/{}_{}_{}'.format(args.model_type, args.loss, datetime.now().strftime('%Y-%m-%d-%H:%M:%S'))

    callbacks = [
        ModelCheckpoint(str(args.checkpoint / 'weights-{epoch:04d}.h5'), save_best_only=True),
        TensorBoard(log_dir=logdir, histogram_freq=1, update_freq='batch'),
        CSVLogger(args.checkpoint / 'history.csv'),
        LearningRateScheduler(schedule, verbose=1)
        # EarlyStopping(patience=4, monitor='val_loss', verbose=2)
                ]
    print("Training starts ...", flush=True)
    hist = model.fit(train_data, validation_data=val_data,
                        callbacks=callbacks,
                        epochs=args.epochs,
                        initial_epoch=args.start_epoch,
                        verbose=args.verbose
                        )

    model.save(args.checkpoint / "final-{}-{}.h5".format(args.model_type, datetime.now().strftime('%Y-%m-%d-%H:%M:%S')))
    hist = pd.DataFrame(hist.history)
    hist.to_csv(args.checkpoint / '{}.csv'.format(datetime.now().strftime('%Y-%m-%d-%H:%M:%S')))


# %%
