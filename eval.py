
#%%

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from skimage.util import montage
import tensorflow as tf

import getdata
import utils

mae = tf.keras.losses.MeanAbsoluteError()
mse = tf.keras.losses.MeanSquaredError()
nmae = lambda y_true, y_pred : np.sum(np.abs(y_pred - y_true)) / np.sum(np.abs(y_true))
nmse = lambda y_true, y_pred : np.sum(np.square(y_pred - y_true)) / np.sum(np.square(y_true))
dice_loss = lambda y_true, y_pred : 1 - (np.sum(y_pred * y_true) + 1) / (np.sum(np.square(y_true)) + np.sum(np.square(y_pred)) + 1)
l1 = lambda x : np.sum(np.abs(x))

mip = lambda x: np.max(x, axis=2)
pro = lambda x: np.sum(x, axis=2)

def hard_threshold(x):
    x[np.logical_and(x < 3, x > -3)] = 0
    return x

# defalut figure size
mpl.rcParams['figure.figsize'] = [10.0, 10.0]


model = tf.keras.models.load_model('weights/rot_prof_RMSprop_NMSE_DICE/weights-0985.h5', compile=False)
rot = True

model.summary()

#%%

data = getdata.LoaderWithDensity('./dataset/invivo')
invivo = True

print(len(data))
for i, f in enumerate(data.files):
    print(i, f)

#%%%
m = 5
x, y, pos, neg = data[m]

if rot:
    x = np.rot90(x, axes=(1, 2))
    y = np.rot90(y, axes=(1, 2))

x = np.expand_dims(x, 0) # + utils.racian_noise((1, 256, 25, 256), 1)
y = np.expand_dims(y, 0)

print(x.shape)

# for oranges
# x = hard_threshold(x)

y_hat = model(x, training=False)
# print(y_hat.shape)

print("MSE: {}".format(mse(y_hat, y).numpy()))
print("MAE: {}".format(mae(y_hat, y).numpy()))
print("NMAE: {}".format(nmae(y_hat, y)))
print("NMSE: {}".format(nmse(y_hat, y)))
print("Dice loss {}".format(dice_loss(y_hat, y)))
print("l1 est: {}".format(l1(y_hat)))
print("l1 gt: {}".format(l1(y)))
print("pred conc: {}".format(y_hat.numpy().max()))
print("gt conc: {}".format(y.max()))

if rot:
    vol_x = np.rot90(np.squeeze(x), k=-1, axes=(1, 2))
    vol_y = np.rot90(np.squeeze(y_hat), k=-1, axes=(1, 2))
    vol_gt = np.rot90(np.squeeze(y), k=-1, axes=(1, 2))
else:
    vol_x = np.squeeze(x)
    vol_y = np.squeeze(y_hat)
    vol_gt = np.squeeze(y)

print(vol_x.shape, vol_y.shape)

if not invivo:
    point = [0 if np.isnan(i) else round(i) for i in utils.center_of_mass(vol_gt)]
    # point = [128, 100, 19]
if invivo:
    point = [0 if np.isnan(i) else round(i) for i in utils.center_of_mass(vol_y)]

print(point)

utils.plot_3d(vol_x, point, "input")
utils.plot_3d(vol_y, point, "estimation")
utils.plot_3d(vol_gt, point, "ground truth")
utils.plot_3d(vol_x, point, "Input and output", vol_y)
utils.plot_3d(neg, point, "Neg img")
utils.plot_3d(pos, point, "Pos img")


plt.figure()
plt.imshow(montage(vol_x.T), cmap='gray')
plt.title("input")
plt.colorbar()
plt.show()

plt.imshow(montage(vol_y.T), cmap='gray')
plt.title("estimation")
plt.colorbar()
plt.show()

plt.imshow(montage(vol_gt.T), cmap='gray')
plt.title("Ground truth")
plt.colorbar()
plt.show()

plt.imshow(montage(vol_x.T), cmap="gray")
plt.imshow(montage(vol_y.T), alpha=0.5)
plt.show()

plt.imshow(mip(vol_y))
plt.title("MIP est")
plt.colorbar()
plt.show()
plt.imshow(mip(vol_gt))
plt.title("MIP gt")
plt.colorbar()
plt.show()
plt.imshow(mip(vol_x))
plt.title("MIP input")
plt.show()

plt.imshow(pro(vol_y))
plt.colorbar()
plt.title("Pro est")
plt.show()
plt.imshow(pro(vol_gt))
plt.title("Pro gt")
plt.colorbar()
plt.show()
plt.imshow(pro(vol_x))
plt.title("Pro input")
plt.show()

# %%
